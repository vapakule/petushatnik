/**
 * Created by Ruslan on 3/31/2016.
 */
'use strict';
angular.module('myApp').controller('userFeedController', ['$scope', '$http', 'PostService','$routeParams', function($scope, $http, PostService,$routeParams) {
	// $http.get('test.json').success(function(data) {
	//     $scope.data = data.entries;
	//     console.log(data.entries);						
	// });
	$scope.targetUser = $routeParams.userName;
	$scope.loaded = 0;
	$scope.defaultAmmount = 20;

	PostService.getUserFeed($scope.defaultAmmount, $scope.loaded, $scope.targetUser).then(function successCallback(response) {
		console.log('success');
		$scope.data = response.data;
		$scope.loaded = response.length;

	}, function errorCallback(response) {
		alert("Epic Failure, please try again");
	});

	$scope.getMore = function() {
		PostService.getUserFeed($scope.defaultAmmount, $scope.loaded, $scope.targetUser).then(function successCallback(response) {
			$scope.updateModel(response);
		}, function errorCallback(response) {
			alert("Epic Failure, please try again");
		});
	}

	$scope.$on('updatePosts',function(){
		console.log("got event userFeedController");
		$scope.getMore();
	})
	$scope.updateModel = function(data) {

		$scope.data = $scope.data.concat(data.data);

		function arrayContains(arr, val, equals) {
			var i = arr.length;
			while (i--) {
				if (equals(arr[i], val)) {
					return true;
				}
			}
			return false;
		}

		function removeDuplicates(arr, equals) {
			var originalArr = arr.slice(0);
			var i, len, j, val;
			arr.length = 0;

			for (i = 0, len = originalArr.length; i < len; ++i) {
				val = originalArr[i];
				if (!arrayContains(arr, val, equals)) {
					arr.push(val);
				}
			}
		}

		function thingsEqual(value1, value2) {
			return value1.id === value2.id;
		}

	removeDuplicates($scope.data, thingsEqual);

	}
}]);