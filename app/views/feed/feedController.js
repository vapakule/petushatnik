/**
 * Created by Ruslan on 3/31/2016.
 */
'use strict';
angular.module('myApp').controller('FeedController', ['$scope', '$http', 'PostService','$rootScope', function($scope, $http, PostService,$rootScope) {
	// $http.get('test.json').success(function(data) {
	//     $scope.data = data.entries;
	//     console.log(data.entries);						
	// });
	$scope.loaded = 0;
	$scope.defaultAmmount = 20;
	$scope.amountOfNew = 0;
	$scope.$on('updatePosts',function(){
		console.log("got event FeedController");
		$scope.getMore()
	})


	PostService.getPosts($scope.defaultAmmount, $scope.loaded).then(function successCallback(response) {
		console.log('success');
		$scope.data = response.data;
		$scope.loaded = response.length;
		$scope.showToast("Great Success!");
	}, function errorCallback(response) {
		$scope.showToast("Epic Failure, please try again");
	});

	$scope.getMore = function() {
		PostService.getPosts($scope.defaultAmmount, $scope.loaded).then(function successCallback(response) {
			$scope.updateModel(response);
			$scope.showToast("You got "+$scope.amountOfNew+" new posts!");
		}, function errorCallback(response) {
			$scope.showToast("Epic Failure, please try again");
		});
	};

	$scope.updateModel = function(data) {

		$scope.data = $scope.data.concat(data.data);
		$scope.amountOfNew = data.data;
		function arrayContains(arr, val, equals) {
			var i = arr.length;
			while (i--) {
				if (equals(arr[i], val)) {
					$scope.amountOfNew--;
					return true;
				}
			}
			return false;
		}

		function removeDuplicates(arr, equals) {
			var originalArr = arr.slice(0);
			var i, len, j, val;
			arr.length = 0;

			for (i = 0, len = originalArr.length; i < len; ++i) {
				val = originalArr[i];
				if (!arrayContains(arr, val, equals)) {
					arr.push(val);
				}
			}
		}

		function thingsEqual(value1, value2) {
			return value1.id === value2.id;
		}

	removeDuplicates($scope.data, thingsEqual);

	}
}]);