angular.module('myApp').factory('PostService', ['$http', function($http) {
	var factory = {};

	factory.postMessage = function(data) {
		data.Title = "we need no title";
		var obj =
			{
				"title": "we need no title",
				"content": data.body

			};
		var headers = {
            'Access-Control-Allow-Credentials': 'true',
        }
		var request = $http({
			method: 'POST',
			url: 'http://kurjatnik.azurewebsites.net/api/Posts',
			headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
			withCredentials: true,
			data: data
		});
		return request;
	}

	factory.getAllFeed = function() {

		var headers = {
            'Access-Control-Allow-Credentials': 'true',
        }

		var request = $http({
			method: 'GET',
			url: 'http://kurjatnik.azurewebsites.net/api/Posts/GetAll',
			headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
			withCredentials: true
		})
		return request;
	}

	factory.getPosts = function(ammount, skip) {

		var obj = {
			"take": ammount,
			"skip": skip

		}
		var headers = {
			'Access-Control-Allow-Credentials': 'true',
		}

		var request = $http({
			method: 'POST',
			url: 'http://kurjatnik.azurewebsites.net/api/Posts/GetWallFeed',
			headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
			withCredentials: true,
			data: obj
		})
		return request;
	}
	
	factory.getUserFeed= function(ammount, skip, userName) {
				var obj = {
			"take": ammount,
			"skip": skip

		}
		var headers = {
			'Access-Control-Allow-Credentials': 'true',
		}

		var request = $http({
			method: 'POST',
			url: 'http://kurjatnik.azurewebsites.net/api/Posts/getUserFeed/' + userName,
			headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
			withCredentials: true,
			data: obj
		})
		return request;
	}
	return factory;
}])
