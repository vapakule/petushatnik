/**
 * Created by Ruslan on 3/30/2016.
 */
'use strict';
angular.module('myApp')
    .controller('MainController', function ($scope, $rootScope, $timeout, $mdSidenav, $mdDialog,$mdBottomSheet, $log, $location, $mdToast, $mdMedia) {
        $scope.neverTryAtHome = $rootScope;
        //SideBar
        $scope.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };
        $scope.toggleLeft = buildDelayedToggler('left');
        $scope.toggleRight = buildToggler('right');
        $scope.isOpenRight = function(){
            return $mdSidenav('right').isOpen();
        };
        $scope.navigateTo = function ( to , event ) {
            console.log("navigateTo:",to,", event:", event );
            $location.path( to );
        };
        $scope.debug = function (text,thing){
            console.debug(text,thing);
        };
        /**
         * Supplies a function that will continue to operate until the
         * time is up.
         */
        function debounce(func, wait, context) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildDelayedToggler(navID) {
            return debounce(function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }
        function buildToggler(navID) {
            return function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }
        }
        //Header
        $scope.header = {
        };
        $scope.cock = 'img/cocks/white-cock.png';
        //SideNav
        $scope.menu = [
            {
                link : '',
                title: 'Dashboard',
                icon: 'dashboard'
            },
            {
                link : '',
                title: 'Friends',
                icon: 'group'
            },
            {
                link : '',
                title: 'Messages',
                icon: 'message'
            }
        ];
        $scope.admin = [
            {
                link : '',
                title: 'Trash',
                icon: 'delete'
            },
            {
                link : 'showListBottomSheet($event)',
                title: 'Settings',
                icon: 'settings'
            }
        ];

        //Modals
        $scope.getHooked = function(ev) {
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'shared/dialogs/login.register/loginRegisterSwitch.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true
                })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        $scope.createPost = function(ev) {
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'shared/dialogs/createPost/createPostDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true
                });
        };
		$scope.rapeCommonSense = function(ev){
			$mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'shared/dialogs/TandC/termsandconditions.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true
                });
		};

        $scope.goBack = function (){
             window.history.back();
        }

        function DialogController($scope, $mdDialog) {
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }

        //Toasts
          $scope.getToastPosition = function() {
            var toastPosition = angular.extend({},{
                bottom: true,
                top: false,
                left: false,
                right: true
            });
            return Object.keys(toastPosition)
                .filter(function(pos) { return toastPosition[pos]; })
                .join(' ');
        };
        $scope.showToast = function(message, position) {
            var toastPos = position || $scope.getToastPosition();
            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position(toastPos)
                    .hideDelay(3000)
            );
        };

        //Custom Reuire to load on demand the required js files.
        $scope.loadScript = function(url, type, charset) {
        if (type===undefined) type = 'text/javascript';
            if (url) {
                var script = document.querySelector("script[src*='"+url+"']");
                if (!script) {
                    var heads = document.getElementsByTagName("head");
                    if (heads && heads.length) {
                        var head = heads[0];
                        if (head) {
                            script = document.createElement('script');
                            script.setAttribute('src', url);
                            script.setAttribute('type', type);
                            if (charset) script.setAttribute('charset', charset);
                            head.appendChild(script);
                        }
                    }
                }
                return script;
            }
        };

        //Fit device media size, based on angular-material layout directive mdMedia
        // Works only with angular-material directives
        $rootScope.media = $mdMedia;
    });